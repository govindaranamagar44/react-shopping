import formatCurrency from "../utl";
// import { useForm } from "react-hook-form";
import React, { useState, useEffect } from "react"
import Fade from "react-reveal/Fade"
import Zoom from "react-reveal/Zoom"
import { useDispatch, useSelector } from "react-redux";
import Modal from "react-modal";
import { removeFromCart } from "../redux/actions/cartActions";
import { clearCart, clearOrder, createOrder, fetchOrders } from "../redux/actions/orderActions";

function Cart(props) {



    const [name, setName] = useState("text");
    const [email, setEmail] = useState("test@test.com");
    const [address, setAddress] = useState("test");

    const [showCheckout, setShowCheckout] = useState(false);


    const cartItems = useSelector(state => state.cart.cartItems);
    const order = useSelector(state => state.order.order);
    const dispatch = useDispatch();

    // const sort = useSelector(state =>state.products.sort);
    // const products = useSelector(state =>state.products.items);
    // const filteredProducts = useSelector(state =>state.products.filteredItems);

    useEffect(() => {
        dispatch(
            fetchOrders()
        );
    }, []);

    const removeFromCartHandle = (item) => (event) => {
        dispatch(removeFromCart(item));
    }


    let temp = [...(cartItems || [])];


    const creatOrderHandle = (e) => {
        if (e && e.preventDefault) {
            e.preventDefault();
            e.persist();
        }
        const order = {
            name: name,
            email: email,
            address: address,
            cartItems: cartItems,
            total: cartItems.reduce((a, c) => a + c.price * c.count, 0)
        }


        dispatch(createOrder(order));
        console.log(order, "orderorder")

    }


    const closeModal = ()  => {
        console.log("closeModalllllllllllll")
        dispatch(clearOrder());
        setShowCheckout(false);
    }

    const formatter = new Intl.DateTimeFormat("en-GB", {
        year: "numeric",
        month: "long",
        day: "2-digit"
      });

    return (
        <div>
            {cartItems?.length === 0 ?
                <div className="cart cart-header"> Cart is empty </div> :
                <div className="cart cart-header"> You have {cartItems?.length} in the cart </div>}
            {
                order && <Modal
                    isOpen={true}
                    onRequestClose={closeModal}
                >
                    <Zoom>
                        <button className="close-modal" onClick={closeModal}>
                            x
                        </button>
                        <div className="order-details">
                            <h3 className="success-message">
                                Your order has been placed.
                            </h3>
                            <h2>
                                Order {order._id}
                            </h2>
                            <ul>
                                <li>
                                    <div>
                                        Name:
                                    </div>
                                    <div>
                                        {order.name}
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        Email:
                                    </div>
                                    <div>
                                        {order.email}
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        Address:
                                    </div>
                                    <div>
                                        {order.address}
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        Date:
                                    </div>
                                    <div>
                                        {order.createdAt}
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        Total:
                                    </div>
                                    <div>
                                        {formatCurrency(order.total)}
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        Cart Items:
                                    </div>
                                    <div>
                                        {order.cartItems.map((x) => (
                                            <div>
                                                {x.count} {" x "} {x.title}
                                            </div>
                                        ))}
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </Zoom>
                </Modal>
            }
            <div>
                <div className="cart">
                    <Fade left cascade>
                        <ul className="cart-items">
                            {cartItems?.map(item => (
                                <li key={item._id}>
                                    <div>
                                        <img src={item.image} alt={item.title} />
                                    </div>
                                    <div>
                                        <div>
                                            {item.title}
                                        </div>
                                        <div className="right">
                                            {formatCurrency(item.price)} x {item.count} {"  "}
                                            <button className="button" onClick={removeFromCartHandle(item)}>Remove</button>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </Fade>
                </div>
                {temp.length !== 0 && (
                    <div>
                        <div className="cart">
                            <div className="total">
                                <div>
                                    Total: {" "}
                                    {formatCurrency(temp.reduce((a, c) => a + (c.price * c.count), 0))}
                                </div>
                                <button onClick={() => { setShowCheckout(true) }} className="button primary">
                                    Proceed
                                </button>
                            </div>
                        </div>
                        {showCheckout && (
                            <Fade right cascade>
                                <div className="cart">
                                    <form onSubmit={creatOrderHandle}>
                                        <ul className="form-container">
                                            <li>
                                                <label>Email</label>
                                                <input
                                                    name="email"
                                                type="email" required  onChange={(e) => setEmail(e.target.value)} />
                                            </li>
                                            <li>
                                                <label>Name</label>
                                                <input
                                                    name="name"
                                                    type="text" required onChange={(e) => setName(e.target.value)} />
                                            </li>
                                            <li>
                                                <label>Address</label>
                                                <input
                                                    name="address"
                                                    type="text" required onChange={(e) => setAddress(e.target.value)} />
                                            </li>
                                            <li>
                                                <button className="button primary" type="submit">
                                                    Checkout
                                                </button>
                                            </li>

                                        </ul>
                                    </form>
                                </div>
                            </Fade>
                        )}
                    </div>
                )}
            </div>
        </div>
    )
}

export default Cart;