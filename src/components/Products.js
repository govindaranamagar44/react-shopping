import formatCurrency from "../utl"
import React, { useState , useEffect} from "react"
import Fade from 'react-reveal/Fade'
import Modal from "react-modal"
import Zoom from "react-reveal/Zoom"
import { useDispatch, useSelector } from "react-redux";
import {fetchProduct} from '../redux/actions/productActions'
import { addToCart } from "../redux/actions/cartActions"
function Products() {
    const [product, setProduct] = useState(null);

    const openModal = (product) =>{
        setProduct(product)
    }
    const closeModal = ()=>{
        setProduct(null)
    }


    const products = useSelector(state => state.products.filteredItems);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProduct());
  }, [ ]);



  const addToCartHandle = (product) => (event) =>{
      console.log(product,"addToCartHandle")
    dispatch(addToCart(product));
  }



  


    return (
        <div>
            <Fade bottom cascade>
                {
                    !products ? (<div>loading...</div>) : 
                    (<ul className="products">
                {
                    products?.map(product => {
                        return (
                            <li key={product._id}>
                                <div className="product">
                                    <a href={"#" + product._id} onClick={()=>openModal(product)}>
                                        <img src={product.image} alt={product.title} />
                                        <p>
                                            {product.title}
                                        </p>
                                    </a>
                                    <div className="product-price">
                                        <div>
                                            {
                                                formatCurrency(product.price)
                                            }
                                        </div>
                                        <button onClick={addToCartHandle(product)} className="button primary">
                                            Add To Cart
                                        </button>
                                    </div>
                                </div>
                            </li>
                        )
                    })
                }
            </ul>)
                }
            </Fade>

            {
                product && (
                    <Modal isOpen onRequestClose={closeModal}>
                        <Zoom>
                            <button className="close-modal" onClick={closeModal}>x</button>
                            <div className="product-details">
                                <img src={product.image} alt={product.title} />
                                <div className="product-details-description">
                                    <p>
                                        <strong>
                                        {product.title}
                                            </strong>
                                    </p>
                                    <p>
                                        {product.description}
                                    </p>
                                    <p>
                                        Available Sizes :{" "}
                                        {product.availableSizes.map(x=>(
                                            <span>{" "} <button className="button">{x}</button></span>
                                        ))}
                                    </p>
                                    <div className="product-price">
                                        <div>
                                            {formatCurrency(product.price)}
                                        </div>
                                        <button className="button primary" onClick={()=>{
                                        addToCart(product)
                                            closeModal();
                                        }}>
                                            Add To Cart
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </Zoom>
                    </Modal>
                )
            }
        </div>
    )
}
export default Products