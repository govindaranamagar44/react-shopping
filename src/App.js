
import React from "react"
import store from "./redux/store"
import {Provider} from "react-redux"
import HomeScreen from "./screens/HomeScreen";
import AdminScreen from "./screens/AdminScreen";
import { BrowserRouter, Route, Link } from "react-router-dom";

function App() {
  // const [stateData, setStateData] = useState(data.products);

  // const [size, setSize] = useState("");
  // const [sort, setSort] = useState("");
  // const [cartItems, setCartItems] = useState(localStorage.getItem("cartItems") ? JSON.parse(localStorage.getItem("cartItems")):[]);


  // const removeFromCart = (product) =>{
  //   let temp = [...cartItems];
  //   setCartItems(temp.filter(x=>x._id !== product._id));
  //   localStorage.setItem(
  //     "cartItems",
  //    JSON.stringify(temp.filter(x=>x._id !== product._id)))
  // }
  

  // const addToCart = (product)=>{
//   let temp = [...cartItems];
//   let alreadyInCart =false;
  //   temp.forEach(item => {
//     if(item._id === product._id ){
  //       item.count++;
  //       alreadyInCart = true;
  //     }
  //   });

    
  //   if(!alreadyInCart){
  //     temp.push({...product, count: 1});
  //   }
  //   setCartItems(temp)
  //   localStorage.setItem("cartItems", JSON.stringify(temp))
  //   console.log(cartItems,'cartcartcart')
  // }

 

 
  // const creatOrder = (order)=>{
  //   alert("need to save order" + order.name)
  // }
  return (
    <Provider store={store}>
      <BrowserRouter>
    <div className="grid-container">
  <header>
          <Link to="/">React Shopping Cart</Link>
          <Link to="/admin">Admin</Link>

      </header>
      <main>
            <Route path="/admin" component={AdminScreen} />
            <Route path="/" component={HomeScreen} exact />
          </main>
      <footer>
        All right is reserved.
      </footer>
    </div>
    </BrowserRouter>
    </Provider>
  );
}

export default App;
